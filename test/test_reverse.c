#include <stdio.h>
#include <stdlib.h>
#include "reverse.h"

int main(int argc, char** argv) {

  char* reversed = reverse(argv[1]);
  printf("%s",reversed);
  free(reversed);
 
  return 0;
}
